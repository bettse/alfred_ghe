# -*- coding: utf-8 -*-
import json
import yaml
import alp
import github

class Search():
    # A list of alp.Item
    results = []

    # For filtering results at the end to add a simple autocomplete
    partial_query = None

    def __init__(self):
        self.settings = alp.Settings()
        self.gh = None

    def _partial_query_filter(self, result):
        """Returns True if the result is valid match for the partial query, else False.

            Args:
                result - an instance of alp.Item
        """
        if self.partial_query:
            return (not result.autocomplete or self.partial_query.lower() in result.autocomplete.lower())
        else:
            return True

    def _filter_results(self):
        self.results = [r for r in self.results if self._partial_query_filter(r)]

    def get_results(self, args):
        """Returns Alfred XML based on the args query.
        """
        query = args[0]
        params = query.split(' ')
        if len(query) > 2:
            self.partial_query = query

        token = self.settings.get('token', '')
        url = self.settings.get('url', '')

        if token and url:
            try:
                #github.enable_console_debug_logging()
                self.gh = github.Github(token, base_url=url)
            except github.GithubException.GithubException, e:
                alp.log("Exception status %s [%s]" % (e.status, e.data))
                return self.result
            user = self.gh.get_user()
            alp.log("Acting as user %s <%s>" % (user.name, user.email) )
            self.process_command(params)

        if not token:
            if len(params) > 1 and params[0] == 'login' and params[1]:
                token = params[1]
                alp.log("token = %s (%s)" % (token, len(token)))
            self.results.append(alp.Item(
                title="login %s" % token,
                subtitle='Set personal access token used to authenticate',
                valid=(token != ''),
                arg="login %s" % token,
                autocomplete='login %s' % token,
                )
            )

        if not url:
            if len(params) > 1 and params[0] == 'set-url' and params[1]:
                url = params[1]
            self.results.append(alp.Item(
                title="set-url %s" % url,
                subtitle='Set GitHub url',
                valid=(url != ''),
                arg="set-url %s" % url,
                autocomplete='set-url %s' % url,
                )
            )

        self._filter_results()
        return self.results

    def process_command(self, params):
        first = params[0]
        user = None
        if first.startswith('@'):
            user = self.gh.get_user(first[1:])
            #self.user_commands(user, params)
        elif first.startswith('my'):
            user = self.gh.get_user()
            self.my_commands(user, params)
        elif '/' in first:
            user, repo = first.split('/', 2)
            self.repo_commands(user, repo, params)

    def repo_commands(self, user_search, repo_search, params):
        try:
            user = self.gh.get_user(user_search)
            repos = user.get_repos()
            for repo in repos:
                if repo.name.startswith(repo_search):
                    title = "%s/%s" % (user.login, repo.name)
                    alp.log(title)
                    self.results.append(alp.Item(
                        title=title,
                        subtitle=repo.description,
                        valid=True,
                        arg="%s" % repo.html_url,
                        )
                    )
        except github.GithubException, e:
            alp.log("Exception status %s [%s]" % (e.status, e.data))
            return


    def my_commands(self, user, params):
        self.results.append(alp.Item(
            title="my profile",
            valid=True,
            arg="%s" % user.html_url,
            autocomplete='my',
            )
        )


if __name__ == '__main__':
    search = Search()
    results = search.get_results(alp.args())
    alp.feedback(results)

# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
