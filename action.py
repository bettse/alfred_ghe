# -*- coding: utf-8 -*-
import datetime
import json
import os
import re
import sys
import time

import alp
from subprocess import call

class AlfredAction:

    def __init__(self):
        self.settings = alp.Settings()
        alp.log("AlfredAction initialized")

    def run_command(self, command):
        parts = command.split(' ')
        alp.log("command parts: %s" % parts)
        if parts[0] == 'login':
          self.settings.set(token=parts[1])
          print('Token set')
        elif parts[0] == 'set-url':
          self.settings.set(url=parts[1])
          print('URL set')
        else:
          call(["open"] + parts)


if __name__ == '__main__':
    action = AlfredAction()
    action.send_filter_query(sys.argv[0])
